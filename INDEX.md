# LABEL

Active Development at [https://github.com/FDOS/label](https://github.com/FDOS/label)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## LABEL.LSM

<table>
<tr><td>title</td><td>LABEL</td></tr>
<tr><td>version</td><td>1.5</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-11-19</td></tr>
<tr><td>description</td><td>Sets or changes the disk volume label</td></tr>
<tr><td>keywords</td><td>label, disk volume</td></tr>
<tr><td>author</td><td>Joe Cosentino &lt;onehardmarine -at- attbi.com&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>andrewbird and PerditionC</td></tr>
<tr><td>primary&nbsp;site</td><td>http://github.com/FDOS/label</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/label/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Label</td></tr>
</table>
